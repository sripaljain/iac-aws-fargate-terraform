provider "aws" {
  region = "ap-southeast-1"
}
terraform {
  backend "s3" {}
}
locals {
  region = "ap-southeast-1"
}

module "my_ecs" {
  // source                  = "../modules/2-ecs/"
  source = "s3::https://terraform-terragrunt-modules.s3-ap-southeast-1.amazonaws.com/AWS-FARGATE/2-ECS" 
  // source                  = "git@gitlab.com:imda_dsl/cloud-infra-terraform-iac/iac-fargate-terraform-modules.git//2-ECS?" 
  # source                = "git::ssh://git@gitlab.com/imda_dsl/cloud-infra-terraform-iac/iac-ecs-fargate-app-module.git//ecs?ref=v0.0.3"
  region                = local.region
  // project               = var.project
  // environment           = var.environment
 remote_state_key = var.remote_state_key
 remote_state_bucket = var.remote_state_bucket
 ecs_domain_name = var.ecs_domain_name
 ecs_cluster_name = var.ecs_cluster_name
 internet_cidr_blocks = var.internet_cidr_blocks
}

variable "remote_state_key" {}
variable "remote_state_bucket" {}
variable "ecs_domain_name" {}
variable "ecs_cluster_name" {}
variable "internet_cidr_blocks" {}


output "vpc_id" {
  value = module.my_ecs.vpc_id
}

output "vpc_cidr_block" {
  value = module.my_ecs.vpc_cidr_block
}

output "ecs_alb_listener_arn" {
  value = module.my_ecs.ecs_alb_listener_arn
}

output "ecs_cluster_name" {
  value = module.my_ecs.ecs_cluster_name
}

output "ecs_cluster_role_name" {
  value = module.my_ecs.ecs_cluster_role_name
}

output "ecs_cluster_role_arn" {
  value = module.my_ecs.ecs_cluster_role_arn
}

output "ecs_domain_name" {
  value = module.my_ecs.ecs_domain_name
}

output "ecs_public_subnets" {
  value = module.my_ecs.ecs_public_subnets
}

output "ecs_private_subnets" {
  value = module.my_ecs.ecs_private_subnets
}

output "alb_dns_name" {
  value = module.my_ecs.alb_dns_name
}

output "alb_zone_id" {
  value = module.my_ecs.alb_zone_id
}

output "route53_ecs_domain_zone_id" {
  value = module.my_ecs.route53_ecs_domain_zone_id
}




