#!/bin/sh
CONFIG="platform-prod.config"
TFVARS="production.tfvars"
if [ "$1" = "initialize" ];then
    echo "Terraform Initiazling........."
    terraform init -backend-config=${CONFIG}
elif [ "$1" = "plan" ];then
    terraform init -backend-config=${CONFIG}
    terraform plan -var-file=${TFVARS}
elif [ "$1" = "deploy" ];then
    terraform init -backend-config=${CONFIG}
    terraform apply -var-file=${TFVARS} -auto-approve
elif [ "$1" = "destroy" ];then
    terraform init -backend-config=${CONFIG}
    terraform destroy -var-file=${TFVARS} -auto-approve
fi