provider "aws" {
  region = "ap-southeast-1"
}
terraform {
  backend "s3" {}
}
locals {
  region = "ap-southeast-1"
}

module "my_vpc" {
  // source                  = "../modules/1-vpc/"
  source = "s3::https://terraform-terragrunt-modules.s3-ap-southeast-1.amazonaws.com/AWS-FARGATE/1-VPC" 
  # source                = "git::ssh://git@gitlab.com/imda_dsl/cloud-infra-terraform-iac/iac-ecs-fargate-app-module.git//ecs?ref=v0.0.3"
  region                = local.region
  project               = var.project
  environment           = var.environment
    az_count                = var.az_count
    vpc_cidr                = var.vpc_cidr
    public_subnet_tag_name  = var.public_subnet_tag_name
    private_subnet_tag_name = var.private_subnet_tag_name
    availability_zones      = var.availability_zones
}

variable "project" {}
variable "environment" {}
variable "az_count"{}
variable "vpc_cidr" {}
variable "public_subnet_tag_name" {}
variable "private_subnet_tag_name" {}
variable "availability_zones" {type        = list(string)}

output "vpc_id" {
  value = module.my_vpc.vpc_id
}

output "vpc_cidr_block" {
  value = module.my_vpc.vpc_cidr_block
}

output "public_subnets" {
 value =  [for az, subnet in module.my_vpc.public_subnets: subnet]
}

output "private_subnets" {
 value =  [for az, subnet in module.my_vpc.private_subnets: subnet]
}