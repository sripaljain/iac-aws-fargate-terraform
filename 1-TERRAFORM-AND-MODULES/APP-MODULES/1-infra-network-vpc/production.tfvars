# VPC variables for production
region = "ap-southeast-1"
project                 = "TerraGrunt"
environment             = "dev"
az_count                = "3"
vpc_cidr                = "10.0.0.0/16"
public_subnet_tag_name  = "TerragruntPaBL"
private_subnet_tag_name = "TerragruntPrTE"
availability_zones      = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]




# public_subnet_1_cidr  = "10.0.1.0/24"
# public_subnet_2_cidr  = "10.0.2.0/24"
# public_subnet_3_cidr  = "10.0.5.0/24"
# private_subnet_1_cidr = "10.0.3.0/24"
# private_subnet_2_cidr = "10.0.4.0/24"
# private_subnet_3_cidr = "10.0.6.0/24"