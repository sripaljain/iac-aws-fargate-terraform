environment                     = ""
remote_state_key                = "PROD/platform.tfstate"
remote_state_bucket             = "terraform-fargate-cluster"
spring_profile                  = "default"
memory                          = 4096 
# User defined variables.....
ecs_service_name                = ""
docker_container_port           = 8501
desired_task_number             = "1"
load_balancer_health_check_path = "/"
