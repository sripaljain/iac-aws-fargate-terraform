locals  {
  domain_name_wo_dots = replace(data.terraform_remote_state.platform.outputs.ecs_domain_name, ".", "-")
  ecs_cluster_name = lower(data.terraform_remote_state.platform.outputs.ecs_cluster_name)
  ecs_service_name = lower(var.ecs_service_name)
  domain_name_with_dots = data.terraform_remote_state.platform.outputs.ecs_domain_name
}
data "terraform_remote_state" "platform" {
  backend = "s3"

  config = {
    key    = var.remote_state_key
    bucket = var.remote_state_bucket
    region = var.region
  }
}

//  new stuff here from platform
data "aws_route53_zone" "ecs_domain" {
  name         = data.terraform_remote_state.platform.outputs.ecs_domain_name
  private_zone = false
}

resource "aws_route53_record" "ecs_load_balancer_record" {
  name    = "${local.ecs_service_name}.${data.terraform_remote_state.platform.outputs.ecs_domain_name}"
  type    = "A"
  zone_id = data.aws_route53_zone.ecs_domain.zone_id

  alias {
    evaluate_target_health = false
    name                   = data.terraform_remote_state.platform.outputs.alb_dns_name
    zone_id                = data.terraform_remote_state.platform.outputs.alb_zone_id
  }
}

# data "template_file" "ecs_task_definition_template" {
#   template = file("task_definition.json")

#   vars = {
#     task_definition_name  = local.ecs_service_name
#     ecs_service_name      = local.ecs_service_name
#     docker_image_url      = var.docker_image_url
#     memory                = var.memory
#     docker_container_port = var.docker_container_port
#     spring_profile        = var.spring_profile
#     region                = var.region
#   }
# }


resource "aws_ecr_repository" "demo-repository" {
  // name                 = local.ecs_service_name
  // name = "mlops-fargate-cluster-python-dev"
  name = format("%s-%s",lower(local.ecs_cluster_name), lower(local.ecs_service_name))
  // name = format("%s-%s", )
  image_tag_mutability = "MUTABLE"
  # image_tag_mutability = "IMMUTABLE"
  tags = {
    //  Name = ${local.ecs_cluster_name}-${var.environment}
    Name = format("%s-%s-%s", local.ecs_cluster_name, local.ecs_service_name,var.environment)
  }
}


resource "aws_ecr_repository_policy" "demo-repo-policy" {
  repository = aws_ecr_repository.demo-repository.name
  policy     = <<EOF
  {
    "Version": "2008-10-17",
    "Statement": [
      {
        "Sid": "adds full ecr access to the demo repository",
        "Effect": "Allow",
        "Principal": "*",
        "Action": [
          "ecr:BatchCheckLayerAvailability",
          "ecr:BatchGetImage",
          "ecr:CompleteLayerUpload",
          "ecr:GetDownloadUrlForLayer",
          "ecr:GetLifecyclePolicy",
          "ecr:InitiateLayerUpload",
          "ecr:PutImage",
          "ecr:UploadLayerPart"
        ]
      }
    ]
  }
  EOF
}

data "aws_ecr_repository" "ecr-respository-image" {
  # name = local.ecs_service_name
  name = aws_ecr_repository.demo-repository.name
  # depends_on = [aws_ecr_repository.demo-repository.name] doesnt work in data 
}

// data "aws_ecr_repository" "ecr-respository-image" {
//   name = local.ecs_service_name
//   # name = aws_ecr_repository.demo-repository.name
//   # depends_on = [aws_ecr_repository.demo-repository.name] doesnt work in data 
// }


resource "aws_ecs_task_definition" "ecsdockerapp-task-definition" {
  # resource "aws_ecs_task_definition" "springbootapp-task-definition" {
  # container_definitions    = data.template_file.ecs_task_definition_template.rendered
  family                   = local.ecs_service_name
  // family = format("%s-%s", local.ecs_service_name,local.domain_name_wo_dots)
  cpu                      = 2048
  memory                   = var.memory
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  execution_role_arn       = aws_iam_role.fargate_iam_role.arn
  task_role_arn            = aws_iam_role.fargate_iam_role.arn
  # "image": "541320134486.dkr.ecr.ap-southeast-1.amazonaws.com/${local.ecs_service_name}:v1",
  container_definitions = <<DEFINITION
 
[
  {
    "name": "${var.ecs_service_name}",
    "image":"${data.aws_ecr_repository.ecr-respository-image.repository_url}:v1",
    "essential": true,
    "environment": [{
      "name": "spring_profile_active",
      "value": "${var.spring_profile}"
    }],
    "portMappings": [{
      "containerPort": ${var.docker_container_port}
    }],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${format("%s-%s",local.ecs_cluster_name, local.ecs_service_name)}-LogGroup",
        "awslogs-region": "${var.region}",
        "awslogs-stream-prefix": "${format("%s-%s",local.ecs_cluster_name, local.ecs_service_name)}-LogGroup-stream"
      }
    }
  }
]
DEFINITION
  tags = {
    // Name = ${local.ecs_cluster_name}-${var.environment}
    Name = format("%s-%s-%s", local.ecs_cluster_name, local.ecs_service_name, var.environment)
  }
}

# resource "aws_ecs_task_definition" "app" {
#   family                   = "${var.app}-${var.environment}"
#   requires_compatibilities = ["FARGATE"]
#   network_mode             = "awsvpc"
#   cpu                      = "256"
#   memory                   = "512"
#   execution_role_arn       = aws_iam_role.ecsTaskExecutionRole.arn

#   # defined in role.tf
#   task_role_arn = aws_iam_role.app_role.arn

#   container_definitions = <<DEFINITION
# [
#   {
#     "name": "${var.container_name}",
#     "image": "${var.default_backend_image}",
#     "essential": true,
#     "portMappings": [
#       {
#         "protocol": "tcp",
#         "containerPort": ${var.container_port},
#         "hostPort": ${var.container_port}
#       }
#     ],
#     "environment": [
#       {
#         "name": "PORT",
#         "value": "${var.container_port}"
#       },
#       {
#         "name": "ENABLE_LOGGING",
#         "value": "false"
#       },
#       {
#         "name": "PRODUCT",
#         "value": "${var.app}"
#       },
#       {
#         "name": "ENVIRONMENT",
#         "value": "${var.environment}"
#       }
#     ],
#     "logConfiguration": {
#       "logDriver": "awslogs",
#       "options": {
#         "awslogs-group": "/fargate/service/${var.app}-${var.environment}",
#         "awslogs-region": "us-east-1",
#         "awslogs-stream-prefix": "ecs"
#       }
#     }
#   }
# ]
# DEFINITION

# }


resource "aws_iam_role" "fargate_iam_role" {
  // name               = "${local.ecs_service_name}-IAM-Role"
  name = format("%s-%s-%s",local.ecs_cluster_name, local.ecs_service_name, "IAM-ROLE")
  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
  {
    "Effect": "Allow",
    "Principal": {
      "Service": ["ecs.amazonaws.com", "ecs-tasks.amazonaws.com"]
    },
    "Action": "sts:AssumeRole"
  }
  ]
}
EOF

}

resource "aws_iam_role_policy" "fargate_iam_role_policy" {
  // name = "${local.ecs_service_name}-IAM-Role-Policy"
  name = format("%s-%s-%s",local.ecs_cluster_name, local.ecs_service_name, "IAM-Role-Policy")
  role = aws_iam_role.fargate_iam_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ecs:*",
        "ecr:*",
        "logs:*",
        "cloudwatch:*",
        "elasticloadbalancing:*"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}


// resource "aws_security_group" "app_security_group" {
//   name        = "${local.ecs_service_name}-SG"
//   description = "Security group for springbootapp to communicate in and out"
//   vpc_id      = data.terraform_remote_state.platform.outputs.vpc_id
//   # vpc_id = "vpc-055d7acdbe2ebdd1d"

//   ingress {
//     from_port   = var.docker_container_port #8501
//     protocol    = "TCP"
//     to_port     = var.docker_container_port
//     cidr_blocks = [data.terraform_remote_state.platform.outputs.vpc_cidr_block]
//     # cidr_blocks = ["10.0.0.0/16"]
//   }

//   egress {
//     from_port   = 0
//     protocol    = "-1"
//     to_port     = 0
//     cidr_blocks = ["0.0.0.0/0"]
//   }

//   # tags = {
//   #   Name = "${local.ecs_service_name}-SG"
//   # }

//   tags = {
//     Name = format("%s-%s-%s", local.ecs_cluster_name, local.ecs_service_name, var.environment)
//   }

// }

resource "aws_alb_target_group" "ecs_app_target_group" {
  // name     = "${local.ecs_service_name}-TG"
  name = format("%s-%s",local.ecs_cluster_name, local.ecs_service_name)
  port     = var.docker_container_port
  protocol = "HTTP"
  vpc_id   = data.terraform_remote_state.platform.outputs.vpc_id
  # vpc_id = "vpc-055d7acdbe2ebdd1d"
  target_type = "ip"

  health_check {
    # path                = "/actuator/health"
    # path                = "/healthy"
    path                = var.load_balancer_health_check_path
    protocol            = "HTTP"
    matcher             = "200"
    interval            = 60
    timeout             = 30
    unhealthy_threshold = "3"
    healthy_threshold   = "3"
  }

  tags = {
    Name = format("%s-%s", local.ecs_service_name, "TG")
  }
}



resource "aws_ecs_service" "ecs_service" {
  name            = local.ecs_service_name
  // name = format("%s-%s", local.ecs_service_name,local.domain_name_wo_dots)
  // name = "asdfasfasfsafdsfsfsfsasfadsfsf"
  task_definition = local.ecs_service_name
  desired_count   = var.desired_task_number
  # cluster         = local.ecs_cluster_name
  cluster = local.ecs_cluster_name
  launch_type = "FARGATE"

  network_configuration {
    // subnets = data.terraform_remote_state.platform.outputs.ecs_private_subnets
    subnets = data.terraform_remote_state.platform.outputs.ecs_public_subnets
    # subnets = [
    #     "subnet-0e7eb6f2ca876e103",
    #     "subnet-0ada882db2685d9a4",
    #     "subnet-074c9203f6fc12965"
    #   ]

    security_groups  = [aws_security_group.app_security_group.id]
    assign_public_ip = true
  }

  load_balancer {
    container_name   = local.ecs_service_name
    container_port   = var.docker_container_port
    target_group_arn = aws_alb_target_group.ecs_app_target_group.arn
  }
  lifecycle {
    // ignore_changes = [task_definition, desired_count]
    ignore_changes = [task_definition]
    # ignore_changes = [desired_count] # gitlab ci
    # ignore_changes = [task_definition, desired_count] # via shell command
  }
  # Name = "${local.ecs_cluster_name}-${var.environment}"
   tags = {
     Url = format("%s.%s", local.ecs_service_name,local.domain_name_with_dots)
   }
}

resource "aws_alb_listener_rule" "ecs_alb_listener_rule" {
  listener_arn = data.terraform_remote_state.platform.outputs.ecs_alb_listener_arn
  # listener_arn = "arn:aws:elasticloadbalancing:ap-southeast-1:541320134486:listener/app/Production-ECS-Cluster-ALB/722ad1549f960534/76d0db45d45c5246"

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.ecs_app_target_group.arn
  }

  condition {
    host_header {
      values = ["${lower(local.ecs_service_name)}.${data.terraform_remote_state.platform.outputs.ecs_domain_name}"]
      # values = ["${lower(local.ecs_service_name)}.vama-dsl.com"]
      #  values = ["${element(values(var.services_map), count.index)}.${var.domain}"]
    }
  }
}

resource "aws_cloudwatch_log_group" "springbootapp_log_group" {
  // name = "${local.ecs_service_name}-LogGroup"
  // name = format("%s-%s", local.ecs_service_name, "LogGroup")
   name = format("%s-%s-%s",local.ecs_cluster_name, local.ecs_service_name, "LogGroup")
}

