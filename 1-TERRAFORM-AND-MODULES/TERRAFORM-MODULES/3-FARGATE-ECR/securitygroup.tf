resource "aws_security_group" "app_security_group" {
  // name        = "${local.ecs_service_name}-SG"
  name = format("%s-%s-%s",data.terraform_remote_state.platform.outputs.ecs_cluster_name, local.ecs_service_name, "SG")
  description = "Security group for springbootapp to communicate in and out"
  vpc_id      = data.terraform_remote_state.platform.outputs.vpc_id
  # vpc_id = "vpc-055d7acdbe2ebdd1d"

  ingress {
    from_port   = var.docker_container_port #8501
    protocol    = "TCP"
    to_port     = var.docker_container_port
    cidr_blocks = [data.terraform_remote_state.platform.outputs.vpc_cidr_block]
    # cidr_blocks = ["10.0.0.0/16"]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  # tags = {
  #   Name = "${local.ecs_service_name}-SG"
  # }

  tags = {
    Name = format("%s-%s-%s", data.terraform_remote_state.platform.outputs.ecs_cluster_name, local.ecs_service_name, var.environment)
  }

}