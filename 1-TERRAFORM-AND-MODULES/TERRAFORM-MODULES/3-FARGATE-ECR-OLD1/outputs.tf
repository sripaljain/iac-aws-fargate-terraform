output "web_url" {
  // value = data.terraform_remote_state.platform.outputs.ecs_domain_name
  value = format("%s.%s", aws_ecs_service.ecs_service.name, data.terraform_remote_state.platform.outputs.ecs_domain_name)
}

output "ecs_domain_name" {
  value = data.terraform_remote_state.platform.outputs.ecs_domain_name
  // value = format("%s.%s", aws_ecs_service.ecs_service.name, data.terraform_remote_state.platform.outputs.ecs_domain_name)
}

output "ecs_service_name" {
  value = aws_ecs_service.ecs_service.name
}

output "alb_dns_name" {
    value = data.terraform_remote_state.platform.outputs.alb_dns_name
}

output "ecs_cluster_name" {
    value = data.terraform_remote_state.platform.outputs.ecs_cluster_name
}

output "aws_ecr_repository" {
  value = aws_ecr_repository.demo-repository.name
}

// output "url" {
//   value = aws_alb_listener_rule.ecs_alb_listener_rule.condition.host_header
// }

// output "web_url {
//   value = [for az, subnet in aws_subnet.private: subnet.id]
// }

