variable "region" {
  default = "ap-southeast-1"
}
variable "project" {

}
variable "environment" {

}

variable "remote_state_key" {
  # default = "PROD/platform.tfstate"
}

variable "remote_state_bucket" {
  # default = "ecs-fargate-terraform-remote-state-jain"
}

#application variables for task
variable "ecs_service_name" {
  # default = "pythonapp5"
}

// variable "ecs_domain_name" {

// }

// variable "docker_image_url" {
//   # default = "541320134486.dkr.ecr.ap-southeast-1.amazonaws.com/pythonapp5:v1"
// }

variable "memory" {
  # default = 1024
}

variable "docker_container_port" {
  # default = 8501
}

variable "spring_profile" {
  # default = "default"
}

variable "desired_task_number" {
  # default = "2"
}
// variable "AWS_ACCESS_KEY_ID" {
// }
// variable "AWS_SECRET_ACCESS_KEY" {
// }
// variable "AWS_DEFAULT_REGION" {
//   # default = "ap-southeast-1"
// }
variable "load_balancer_health_check_path" {
  # default = "/"
}

