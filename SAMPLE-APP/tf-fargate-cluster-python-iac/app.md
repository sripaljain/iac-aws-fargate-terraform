## Demo Site

### Local
 * In browser, open `localhost:8501`

```bash
pip install -r requirements.txt
streamlit run app.py
```

### Docker
 * In browser, open `localhost:3000`

```bash
docker build -t demo .
docker run -d -p 3000:8501 --log-opt max-size=5m --log-opt max-file=5 --restart always --name demo demo
```