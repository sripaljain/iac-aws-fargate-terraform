# Terraform deploying a sample python application to AWS Fargate

## Usage

```
 Step1: change variable for prod-deploy-python.sh [PROD-OR-DEV=dev or PROD-OR-DEV=prod]
 Step2: sh prod-deploy-python.sh plan
 Step3: sh prod-deploy-python.sh deploy
 Step4: sh prod-deploy-python.sh dockerize
 Step5: sh prod-deploy-python.sh destroy
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.12.21 |
| aws | >= 2.0.55 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 2.0 |


## CHANGING OF HOST HEADER FROM python-prod.dsldemo.site to >> demo-dev.dsldemo.site
TWO WAYS
1. VIA SHELL SCRIPT
    a. VARIABLE PROD_OR_DEV=dev
    b. VARIABLE SERVICE_NAME="python-${PROD_OR_DEV}" to SERVICE_NAME="demo-${PROD_OR_DEV}"
    c. sh prod-deploy-python.sh plan
    d. sh prod-deploy-python.sh deploy
    e. sh prod-deploy-python.sh dockerize
    f. sh prod-deploy-python.sh destroy

2. VIA GITLAB-CICD
    a. VARIABLE "SERVICE_NAME_DEV: demo-dev"
    b. VARIABLE SERVICE_NAME="python-${PROD_OR_DEV}" to SERVICE_NAME="demo-${PROD_OR_DEV}"
    c. sh prod-deploy-python.sh plan
    d. sh prod-deploy-python.sh deploy
    e. sh prod-deploy-python.sh dockerize
    f. sh prod-deploy-python.sh destroy