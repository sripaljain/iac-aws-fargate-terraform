const express = require('express');
const bodyParser = require('body-parser');
const favicon = require('serve-favicon')
const path = require('path')
const app = express();
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));
app.get('/', (req, res) => {
  res.sendFile('public/index.html');
});
app.listen(3000);