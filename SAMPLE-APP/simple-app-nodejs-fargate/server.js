const express = require('express')
const app = express();
const PORT = 8501;
const bodyParser = require('body-parser');
var favicon = require('serve-favicon')
var path = require('path')

// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));
// app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.get('/', (req,res,next)=>{
    // res.status(200).json({
    //     message: "hello world Fargate V4"
    // })
    res.sendFile(__dirname + '/public/index.html');
})

// app.use(express.static(__dirname + "/views"))
app.get('/healthy', (req,res,next)=>{
    res.status(200).json({
        message: "Health Check Passed"
    })
})

app.get('/test', (req,res,next)=>{
    res.status(200).json({
        message: "Health Check Passed"
    })
})

app.listen(PORT, function(){
    console.log(`Magik happens on port ${PORT}`)
})