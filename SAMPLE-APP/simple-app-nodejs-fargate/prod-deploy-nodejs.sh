#!/bin/sh
set -e
PROD_OR_DEV="prod"
DOCKER_CONTAINER_PORT=8501
LOAD_BALANCER_HEALTH_CHECK_PATH="/"
SERVICE_NAME="node-${PROD_OR_DEV}" # rake note in app.tf file of ecs_servicaname othersise endup debuggng

if [ "$PROD_OR_DEV" = "prod" ]; then
    echo "Prod selection"
elif [ "$PROD_OR_DEV" = "dev" ]; then
    echo "Dev selection"
else
    echo "must have either prod or dev selection, exiting...."
    exit 1
fi
# if [ -z "$STAGE" ]; then
#     echo "STAGE VAIRABLE NOT SET PROPERLY exiting the script..."
#     exit 1
# fi
if [ -z "$PROD_OR_DEV" ]; then
    echo "STAGE VAIRABLE NOT SET PROPERLY exiting the script..."
    exit 1
fi

# S3_BUCKET_NAME="terraform-terragrunt-modules/AWS-FARGATE/TERRAGRUNT/3-fargate-application/"
# S3_BUCKET_NAME="terraform-terragrunt-modules/MLOPS-FARGATE/3-fargate-application"
S3_BUCKET_NAME="terraform-terragrunt-modules/2-MLOPS-FARGATE-CLUSTER/3-fargate-application"
ECS_CLUSTER_NAME="mlops-fargate"
DOMAIN_BASE_NAME="dsldemo.site"
S3_TD_DIR="TERRAFORM_INFRA"
TERRAFORM_DIR="TERRAFORM_INFRA"/${PROD_OR_DEV}
# FARGATE_CLUSTER_SERVICE_NAME=${SERVICE_NAME}-${DOMAIN_BASE_NAME}
FARGATE_CLUSTER_SERVICE_NAME=${SERVICE_NAME}
SERVICE_TAG="v1"
# ECR_REPO_URL="351555610119.dkr.ecr.eu-west-1.amazonaws.com/${SERVICE_NAME}"
ECR_REPO_URL="541320134486.dkr.ecr.ap-southeast-1.amazonaws.com/${ECS_CLUSTER_NAME}-${SERVICE_NAME}" #mlops-fargate-cluster-python-dev 

echo $ECR_REPO_URL
AWS_DEFAULT_REGION="ap-southeast-1"

DOWNLOAD_TERRAFORM_CODE_FROM_S3_BUCKET() {
    # rm -rf ${S3_TD_DIR}
    aws s3 cp s3://${S3_BUCKET_NAME}/${S3_TD_DIR} ${S3_TD_DIR} --recursive --exclude "*.sh" --exclude "*.md"
    cd "$TERRAFORM_DIR"
    rm -rf .terraform*
    # echo 'key="PROD/APP/'$SERVICE_NAME'.dsldemo.site.tfstate"' >app-prod.config
    echo 'key="PROD/APP/'$SERVICE_NAME'.'$DOMAIN_BASE_NAME'.tfstate"' >app-prod.config
    echo 'bucket="terraform-fargate-cluster"' >> app-prod.config
    echo 'region="ap-southeast-1"' >> app-prod.config
    # sed -E -i.bak "s/^(ecs_service_name[[:blank:]]*=[[:blank:]]*).*/\1\"${SERVICE_NAME}\"/" production.tfvars && rm production.tfvars.bak
    # sed -E -i.bak "s/^(environment[[:blank:]]*=[[:blank:]]*).*/\1\"${SERVICE_NAME}\"/" production.tfvars && rm production.tfvars.bak
    # sed -E -i.bak "s/^(docker_container_port[[:blank:]]*=[[:blank:]]*).*/\1${CONTAINER_PORT}/" production.tfvars && rm production.tfvars.bak
    awk '!/ecs_service_name/' production.tfvars > tmpfile && mv tmpfile production.tfvars
    awk '!/environment/' production.tfvars > tmpfile && mv tmpfile production.tfvars
    awk '!/docker_container_port/' production.tfvars > tmpfile && mv tmpfile production.tfvars
    awk '!/load_balancer_health_check_path/' production.tfvars > tmpfile && mv tmpfile production.tfvars
    echo "ecs_service_name = \"$SERVICE_NAME\"" >> production.tfvars
    echo "environment = \"$PROD_OR_DEV\"" >> production.tfvars
    echo "docker_container_port = $DOCKER_CONTAINER_PORT" >> production.tfvars 
    echo "load_balancer_health_check_path = \"$LOAD_BALANCER_HEALTH_CHECK_PATH\"" >> production.tfvars
    terraform fmt
} 

if [ "$1" = "dockerize" ];then
    echo "Cloudanizing the node application..."
    # $(aws ecr get-login --no-include-email --region ap-southeast-1) => odnt use this old method    
    aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 541320134486.dkr.ecr.ap-southeast-1.amazonaws.com
    # aws ecr create-repository --repository-name ${SERVICE_NAME:?} || true
    docker build -t ${SERVICE_NAME}:${SERVICE_TAG} .
    # docker tag media:latest 541320134486.dkr.ecr.ap-southeast-1.amazonaws.com/media:latest
    docker tag ${SERVICE_NAME}:${SERVICE_TAG} ${ECR_REPO_URL}:${SERVICE_TAG}
    docker push ${ECR_REPO_URL}:${SERVICE_TAG}
    docker system prune -a -f
elif [ "$1" = "plan" ];then
    DOWNLOAD_TERRAFORM_CODE_FROM_S3_BUCKET
    terraform init -backend-config="app-prod.config"
    terraform plan -var-file="production.tfvars"
elif [ "$1" = "deploy" ];then
    DOWNLOAD_TERRAFORM_CODE_FROM_S3_BUCKET
    terraform init -backend-config="app-prod.config"
    #  terraform taint -allow-missing aws_ecs_task_definition.${SERVICE_NAME}-task-definition
    terraform apply -var-file="production.tfvars" -auto-approve
elif [ "$1" = "destroy" ];then
    DOWNLOAD_TERRAFORM_CODE_FROM_S3_BUCKET
    terraform init -backend-config="app-prod.config"
    # terraform state rm aws_ecr_repository.demo-repository
    # terraform destroy -var-file="production.tfvars" -var "docker_image_url=${ECR_REPO_URL}:${SERVICE_TAG}" -auto-approve
     terraform destroy -var-file="production.tfvars" -auto-approve
elif [ "$1" = "redeploy" ];then
    # cd $DOCKER_PGM_DIRECTORY
    echo "Cloudanizing the node application..."
    aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 541320134486.dkr.ecr.ap-southeast-1.amazonaws.com
    aws ecr create-repository --repository-name ${SERVICE_NAME:?} || true
    docker build -t ${SERVICE_NAME}:${SERVICE_TAG} .
    docker tag ${SERVICE_NAME}:${SERVICE_TAG} ${ECR_REPO_URL}:${SERVICE_TAG}
    docker push ${ECR_REPO_URL}:${SERVICE_TAG}
    aws ecs update-service --cluster $ECS_CLUSTER_NAME --service $SERVICE_NAME --force-new-deployment --region $AWS_DEFAULT_REGION
fi


if [ "$1" = "GodModeOn" ];then
    DOWNLOAD_TERRAFORM_CODE_FROM_S3_BUCKET
    echo "Initializing terraform and statemanagement....."
    terraform init -backend-config="app-prod.config"
    # terraform plan -var-file="production.tfvars" #-var "docker_image_url=${ECR_REPO_URL}:${SERVICE_TAG}"
    echo "Terraform deploying infra >>>>>>>>>>....."
    terraform apply -var-file="production.tfvars" -auto-approve
    cd "../../"
    echo "Cloudanizing the node application....."
    aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 541320134486.dkr.ecr.ap-southeast-1.amazonaws.com
    # aws ecr create-repository --repository-name ${SERVICE_NAME:?} || true
    echo "Dockerizing >>>>>>>>>>......."
    docker build -t ${SERVICE_NAME}:${SERVICE_TAG} .
    docker tag ${SERVICE_NAME}:${SERVICE_TAG} ${ECR_REPO_URL}:${SERVICE_TAG}
    docker push ${ECR_REPO_URL}:${SERVICE_TAG}
    echo "Fargate Deployement......"
    aws ecs update-service --cluster $ECS_CLUSTER_NAME --service $SERVICE_NAME --force-new-deployment --region $AWS_DEFAULT_REGION
fi

if [ "$1" = "c" ];then
    cd "$TERRAFORM_DIR"
        echo 'key="PROD/APP/'$SERVICE_NAME'.dsldemo.site.tfstate'  >a.config
        echo 'bucket="terraform-fargate-cluster"'  >>a.config 
        echo 'region="ap-southeast-1"' >> a.config 
    echo "Cloudanizing the node application..."
fi


if [ "$1" = "l" ];then
    cd "$TERRAFORM_DIR"
    terraform output web_url
fi


# replaceValue=SQLTEST
# sed -i '/\[ecs_service_name     \]/d' /TERRAFORM_INFRA/prod/production.tfvars
# sed -i "/ecs_service_name =/d" production.tfvars
# echo "ecs_service_name = $replaceValue"  >> /TERRAFORM_INFRA/prod/production.tfvars
# sed -E -i.bak "s/^(ecs_service_name[[:blank:]]*=[[:blank:]]*).*/\1\"${SERVICE_NAME}\"/" production.tfvars && rm production.tfvars.bak
#  awk '!/ecs_service_name/' b.tfvars > tmpfile && mv tmpfile production.tfvars
#  echo "ecs_service_name = $replaceValue" >> production.tfvars

















# #!/bin/sh
# set -e
# PROD_OR_DEV="prod"

# if [ "$PROD_OR_DEV" = "prod" ]; then
#     echo "Prod selection"
# elif [ "$PROD_OR_DEV" = "dev" ]; then
#     echo "Dev selection"
# else
#     echo "must have either prod or dev selection, exiting...."
#     exit 1
# fi
# if [ -z "$PROD_OR_DEV" ]; then
#     echo "STAGE VAIRABLE NOT SET PROPERLY exiting the script..."
#     exit 1
# fi
# S3_BUCKET_NAME="terraform-terragrunt-modules/AWS-FARGATE/TERRAGRUNT/3-fargate-application/"
# TERRAFORM_DIR="TERRAFORM_INFRA"/${PROD_OR_DEV}
# SERVICE_NAME="nodeapp-${PROD_OR_DEV}" # rake note in app.tf file of ecs_servicaname othersise endup debuggng
# DOCKER_PGM_DIRECTORY="simple-app-nodejs-fargate"
# SERVICE_TAG="v1"
# ECR_REPO_URL="541320134486.dkr.ecr.ap-southeast-1.amazonaws.com/${SERVICE_NAME}"
# ECS_CLUSTER_NAME="Production-Fargate-Cluster"
# AWS_DEFAULT_REGION="ap-southeast-1"

# # https://nodeapp-prod.vama-dsl.com
# if [ "$1" = "dockerize" ];then
#     # cd $DOCKER_PGM_DIRECTORY
#     echo "Cloudanizing the node application..."
#     echo $SERVICE_NAME
#     aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 541320134486.dkr.ecr.ap-southeast-1.amazonaws.com
#     # aws ecr create-repository --repository-name ${SERVICE_NAME:?} || true
#     docker build -t ${SERVICE_NAME}:${SERVICE_TAG} .
#     docker tag ${SERVICE_NAME}:${SERVICE_TAG} ${ECR_REPO_URL}:${SERVICE_TAG}
#     docker push ${ECR_REPO_URL}:${SERVICE_TAG}
# elif [ "$1" = "plan" ];then
#     cd ${TERRAFORM_DIR}
#     echo 'key="PROD/APP/'$SERVICE_NAME'.dsldemo.site.tfstate"' >app-prod.config
#     echo 'bucket="terraform-fargate-cluster"' >> app-prod.config
#     echo 'region="ap-southeast-1"' >> app-prod.config
#     sed -E -i.bak "s/^(ecs_service_name[[:blank:]]*=[[:blank:]]*).*/\1\"${SERVICE_NAME}\"/" production.tfvars && rm production.tfvars.bak
#     sed -E -i.bak "s/^(environment[[:blank:]]*=[[:blank:]]*).*/\1\"${PROD_OR_DEV}\"/" production.tfvars && rm production.tfvars.bak
#     aws s3 cp s3://${S3_BUCKET_NAME} . --recursive --exclude "*.sh" --exclude "*.md"
#     terraform init -backend-config="app-prod.config"
#     # terraform taint -allow-missing aws_ecs_task_definition.ecsdockerapp-task-definition
#     terraform plan -var-file="production.tfvars" #-var "docker_image_url=${ECR_REPO_URL}:${SERVICE_TAG}"
# elif [ "$1" = "deploy" ];then
#     cd $TERRAFORM_DIR
#     echo 'key="PROD/APP/'$SERVICE_NAME'.dsldemo.site.tfstate"' >app-prod.config
#     echo 'bucket="terraform-fargate-cluster"' >> app-prod.config
#     echo 'region="ap-southeast-1"' >> app-prod.config
#     sed -E -i.bak "s/^(ecs_service_name[[:blank:]]*=[[:blank:]]*).*/\1\"${SERVICE_NAME}\"/" production.tfvars && rm production.tfvars.bak
#     sed -E -i.bak "s/^(environment[[:blank:]]*=[[:blank:]]*).*/\1\"${PROD_OR_DEV}\"/" production.tfvars && rm production.tfvars.bak
#     # cd "../$TERRAFORM_DIR"
#     aws s3 cp s3://$S3_BUCKET_NAME . --recursive --exclude "*.sh" --exclude "*.md"
#     terraform init -backend-config="app-prod.config"
#     # terraform taint -allow-missing aws_ecs_task_definition.ecsdockerapp-task-definition
#     # terraform apply -var-file="production.tfvars" #-var "docker_image_url=${ECR_REPO_URL}:${SERVICE_TAG}" -auto-approve
#     terraform apply -var-file="production.tfvars" -auto-approve
# elif [ "$1" = "destroy" ];then
#     # cd $TERRAFORM_DIR
#     cd ${TERRAFORM_DIR}
#     echo 'key="PROD/APP/'$SERVICE_NAME'.dsldemo.site.tfstate"' >app-prod.config
#     echo 'bucket="terraform-fargate-cluster"' >> app-prod.config
#     echo 'region="ap-southeast-1"' >> app-prod.config
#     sed -E -i.bak "s/^(ecs_service_name[[:blank:]]*=[[:blank:]]*).*/\1\"${SERVICE_NAME}\"/" production.tfvars && rm production.tfvars.bak
#     sed -E -i.bak "s/^(environment[[:blank:]]*=[[:blank:]]*).*/\1\"${PROD_OR_DEV}\"/" production.tfvars && rm production.tfvars.bak
#     aws s3 cp s3://$S3_BUCKET_NAME . --recursive --exclude "*.sh" --exclude "*.md"
#     terraform init -backend-config="app-prod.config"
#     # terraform destroy -var-file="production.tfvars" -var "docker_image_url=${ECR_REPO_URL}:${SERVICE_TAG}" -auto-approve
#     terraform destroy -var-file="production.tfvars" -auto-approve
# elif [ "$1" = "dummy" ];then
#     echo "all good"
# elif [ "$1" = "redeploy" ];then
#     # cd $DOCKER_PGM_DIRECTORY
#     echo "Cloudanizing the node application..."
#     aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 541320134486.dkr.ecr.ap-southeast-1.amazonaws.com
#     aws ecr create-repository --repository-name ${SERVICE_NAME:?} || true
#     docker build -t ${SERVICE_NAME}:${SERVICE_TAG} .
#     docker tag ${SERVICE_NAME}:${SERVICE_TAG} ${ECR_REPO_URL}:${SERVICE_TAG}
#     docker push ${ECR_REPO_URL}:${SERVICE_TAG}
#     aws ecs update-service --cluster $ECS_CLUSTER_NAME --service $SERVICE_NAME --force-new-deployment --region $AWS_DEFAULT_REGION
# fi

# if [ "$1" = "list" ];then
#     cd $TERRAFORM_DIR
#     # terraform init -backend-config="app-prod.config"
#     terraform state show module.my_3-fargate-ecr.aws_alb_listener_rule.ecs_alb_listener_rule #-var-file="production.tfvars" module.my_3-fargate-ecr.aws_alb_listener_rule.ecs_alb_listener_rule
# fi

# if [ "$1" = "GodModeOn" ];then
#     cd ${TERRAFORM_DIR}
#     aws s3 cp s3://$S3_BUCKET_NAME . --recursive --exclude "*.sh" --exclude "*.md"
#     echo "Initializing terraform and statemanagement....."
#     terraform init -backend-config="app-prod.config"
#     terraform plan -var-file="production.tfvars" #-var "docker_image_url=${ECR_REPO_URL}:${SERVICE_TAG}"
#     echo "Terraform deploying infra >>>>>>>>>>....."
#     terraform apply -var-file="production.tfvars" -auto-approve
#     cd "../../"
#     echo "Cloudanizing the node application....."
#     aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 541320134486.dkr.ecr.ap-southeast-1.amazonaws.com
#     # aws ecr create-repository --repository-name ${SERVICE_NAME:?} || true
#     echo "Dockerizing >>>>>>>>>>......."
#     docker build -t ${SERVICE_NAME}:${SERVICE_TAG} .
#     docker tag ${SERVICE_NAME}:${SERVICE_TAG} ${ECR_REPO_URL}:${SERVICE_TAG}
#     docker push ${ECR_REPO_URL}:${SERVICE_TAG}
#     echo "Fargate Deployement......"
#     aws ecs update-service --cluster $ECS_CLUSTER_NAME --service $SERVICE_NAME --force-new-deployment --region $AWS_DEFAULT_REGION
# fi

# if [ "$1" = "show" ];then
#     cd $TERRAFORM_DIR
#     # terraform init -backend-config="app-prod.config"
#         # terraform state show module.my_3-fargate-ecr.aws_alb_listener_rule.ecs_alb_listener_rule #-var-file="production.tfvars" module.my_3-fargate-ecr.aws_alb_listener_rule.ecs_alb_listener_rule

#     terraform state show
# fi

# # #!/bin/sh
# # set -e
# # PROD_OR_DEV="dev"

# # if [ "$PROD_OR_DEV" = "prod" ]; then
# #     echo "Prod selection"
# # elif [ "$PROD_OR_DEV" = "dev" ]; then
# #     echo "Dev selection"
# # else
# #     echo "must have either prod or dev selection, exiting...."
# #     exit 1
# # fi
# # if [ -z "$PROD_OR_DEV" ]; then
# #     echo "STAGE VAIRABLE NOT SET PROPERLY exiting the script..."
# #     exit 1
# # fi
# # TERRAFORM_DIR="TERRAFORM_INFRA"/${PROD_OR_DEV}
# # SERVICE_NAME="nodeapp-${PROD_OR_DEV}" # rake note in app.tf file of ecs_servicaname othersise endup debuggng
# # DOCKER_PGM_DIRECTORY="simple-app-nodejs-fargate"
# # SERVICE_TAG="v1"
# # ECR_REPO_URL="541320134486.dkr.ecr.ap-southeast-1.amazonaws.com/${SERVICE_NAME}"
# # ECS_CLUSTER_NAME="Production-Fargate-Cluster"
# # AWS_DEFAULT_REGION="ap-southeast-1"

# # # https://nodeapp-prod.vama-dsl.com
# # if [ "$1" = "dockerize" ];then
# #     # cd $DOCKER_PGM_DIRECTORY
# #     echo "Cloudanizing the node application..."
# #     echo $SERVICE_NAME
# #     aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 541320134486.dkr.ecr.ap-southeast-1.amazonaws.com
# #     # aws ecr create-repository --repository-name ${SERVICE_NAME:?} || true
# #     docker build -t ${SERVICE_NAME}:${SERVICE_TAG} .
# #     docker tag ${SERVICE_NAME}:${SERVICE_TAG} ${ECR_REPO_URL}:${SERVICE_TAG}
# #     docker push ${ECR_REPO_URL}:${SERVICE_TAG}
# # elif [ "$1" = "plan" ];then
# #     # cd ${TERRAFORM_DIR}/${PROD_OR_DEV}
# #     cd ${TERRAFORM_DIR}
# #     terraform init -backend-config="app-prod.config"
# #     # terraform taint -allow-missing aws_ecs_task_definition.ecsdockerapp-task-definition
# #     terraform plan -var-file="production.tfvars" #-var "docker_image_url=${ECR_REPO_URL}:${SERVICE_TAG}"
# # elif [ "$1" = "deploy" ];then
# #     cd $TERRAFORM_DIR
# #     # cd "../$TERRAFORM_DIR"
# #     terraform init -backend-config="app-prod.config"
# #     # terraform taint -allow-missing aws_ecs_task_definition.ecsdockerapp-task-definition
# #     # terraform apply -var-file="production.tfvars" #-var "docker_image_url=${ECR_REPO_URL}:${SERVICE_TAG}" -auto-approve
# #     terraform apply -var-file="production.tfvars" -auto-approve
# # elif [ "$1" = "destroy" ];then
# #     # cd $TERRAFORM_DIR
# #     cd ${TERRAFORM_DIR}
# #     terraform init -backend-config="app-prod.config"
# #     # terraform destroy -var-file="production.tfvars" -var "docker_image_url=${ECR_REPO_URL}:${SERVICE_TAG}" -auto-approve
# #     terraform destroy -var-file="production.tfvars" -auto-approve
# # elif [ "$1" = "dummy" ];then
# #     echo "all good"
# # elif [ "$1" = "redeploy" ];then
# #     # cd $DOCKER_PGM_DIRECTORY
# #     echo "Cloudanizing the node application..."
# #     aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 541320134486.dkr.ecr.ap-southeast-1.amazonaws.com
# #     aws ecr create-repository --repository-name ${SERVICE_NAME:?} || true
# #     docker build -t ${SERVICE_NAME}:${SERVICE_TAG} .
# #     docker tag ${SERVICE_NAME}:${SERVICE_TAG} ${ECR_REPO_URL}:${SERVICE_TAG}
# #     docker push ${ECR_REPO_URL}:${SERVICE_TAG}
# #     aws ecs update-service --cluster $ECS_CLUSTER_NAME --service $SERVICE_NAME --force-new-deployment --region $AWS_DEFAULT_REGION
# # fi