provider "aws" {
  region = "ap-southeast-1"
    version = "~> 2.0"
}
terraform {
  backend "s3" {}
}
locals {
  region = "ap-southeast-1"
}

module "my_3-fargate-ecr" {
  // source = "../../iac-ecs-fargate-app-module"
  // source = "s3::https://terraform-terragrunt-modules.s3-ap-southeast-1.amazonaws.com/AWS-FARGATE/3-FARGATE-ECR" 
  // source = "../../../../1-TERRAFORM-AND-MODULES/TERRAFORM-MODULES/3-FARGATE-ECR"
  // source = "s3::https://terraform-terragrunt-modules.s3-ap-southeast-1.amazonaws.com/1-TERRAFORM-AND-MODULES/TERRAFORM-MODULES/3-FARGATE-ECR"
  source = "s3::https://terraform-terragrunt-modules.s3-ap-southeast-1.amazonaws.com/1-TERRAFORM-AND-MODULES/TERRAFORM-MODULES/3-FARGATE-ECR"

  // source                  = "git@gitlab.com:imda_dsl/cloud-infra-terraform-iac/iac-fargate-terraform-modules.git//3-fargate-ecr?" 
  # source                = "git::ssh://git@gitlab.com/imda_dsl/cloud-infra-terraform-iac/iac-ecs-fargate-app-module.git//ecs?ref=v0.0.3"
  region                          = local.region
  environment                     = var.environment
  remote_state_key                = var.remote_state_key
  remote_state_bucket             = var.remote_state_bucket
  // ecs_domain_name                 = var.ecs_domain_name
  ecs_service_name                = lower(var.ecs_service_name)
  docker_container_port           = var.docker_container_port
  desired_task_number             = var.desired_task_number
  memory                          = var.memory
  spring_profile                  = var.spring_profile
  load_balancer_health_check_path = var.load_balancer_health_check_path
}

variable "environment" {}
variable "remote_state_key" {}
variable "remote_state_bucket" {}
// variable "ecs_domain_name" {}
variable "ecs_service_name" {}
variable "docker_container_port" {}
variable "desired_task_number" {}
variable "spring_profile" {}
variable "memory" {}
variable "load_balancer_health_check_path" {}


output "web_url" {
    value = module.my_3-fargate-ecr.web_url
}

output "ecs_service_name" {
  value = module.my_3-fargate-ecr.ecs_service_name
}
output "ecs_domain_name" {
  value = module.my_3-fargate-ecr.ecs_domain_name
}

output "alb_dns_name" {
    value = module.my_3-fargate-ecr.alb_dns_name
}

output "ecs_cluster_name" {
    value = module.my_3-fargate-ecr.ecs_cluster_name
}

output "aws_ecr_repository" {
  value = module.my_3-fargate-ecr.aws_ecr_repository
}


output "aws_security_group" {
  value = module.my_3-fargate-ecr.aws_security_group
}

output "aws_iam_role" {
  value = module.my_3-fargate-ecr.aws_iam_role
}

output "aws_cloudwatch_log_group" {
  value = module.my_3-fargate-ecr.aws_cloudwatch_log_group
}