# remote state
remote_state_key    = "PROD/infrastructure.tfstate"
remote_state_bucket = "terraform-fargate-cluster"
# ecs_domain_name      = "baba.dsldemo.site"
# ecs_domain_name      = "*.dsldemo.site"
#ecs_domain_name      = "vama-dsl.com"
#ecs_domain_name = "dcai-dsl.com"
ecs_domain_name =  "dsldemo.site"
// below clustername should be shorter e.g "mlops-fargate", dont create long names. 
// as aws names throws out an error
ecs_cluster_name     = "mlops-fargate"
internet_cidr_blocks = "0.0.0.0/0"