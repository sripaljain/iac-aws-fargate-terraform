data "aws_route53_zone" "ecs_domain" {
  name         = var.ecs_domain_name
  private_zone = false
}

data "aws_acm_certificate" "ecs_domain_certificate" {
  domain      = "*.${var.ecs_domain_name}"
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}

// below is forself signed certificate similar to what i used fir lufthansa domain
// resource "tls_private_key" "example" {
//   algorithm = "RSA"
// }

// resource "tls_self_signed_cert" "example" {
//   key_algorithm   = "RSA"
//   private_key_pem = tls_private_key.example.private_key_pem

//   subject {
//     common_name  = "vama-dsl.com"
//     organization = "ACME Examples, Inc"
//   }

//   validity_period_hours = 12

//   allowed_uses = [
//     "key_encipherment",
//     "digital_signature",
//     "server_auth",
//   ]
// }

// resource "aws_acm_certificate" "ecs_domain_certificate" {
//   private_key      = tls_private_key.example.private_key_pem
//   certificate_body = tls_self_signed_cert.example.cert_pem
// }