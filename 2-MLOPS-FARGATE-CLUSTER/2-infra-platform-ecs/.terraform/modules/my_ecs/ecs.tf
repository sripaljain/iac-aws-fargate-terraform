// provider "aws" {

//   # Write the region name below in which your environment has to be deployed!
//   region                  = "ap-southeast-1"
//   shared_credentials_file = "~/.aws/credentials"
//   # access_key = ""
//   # secret_key = ""
//   profile = "default"
// }
// terraform {
//   backend "s3" {}
// }
# terraform {
#   backend "s3" {
#     access_key = ""
#     secret_key = ""
#     bucket     = "ecs-fargate-terraform-remote-state-jain"
#     key        = "PROD/platform.tfstate"
#     region     = "ap-southeast-1"
#   }
# }
data "terraform_remote_state" "infrastructure" {
  backend = "s3"

  config = {
    region = var.region
    bucket = var.remote_state_bucket
    key    = var.remote_state_key
  }
}
# data "terraform_remote_state" "infrastructure" {
#      backend =  "s3" 
#      config {
#     access_key = ""
#     secret_key = ""
#     bucket     = "ecs-fargate-terraform-remote-state-jain"
#     key        = "PROD/platform.tfstate"
#     region     = "ap-southeast-1"
#   }
# }
resource "aws_ecs_cluster" "production-fargate-cluster" {
  name = var.ecs_cluster_name
}

resource "aws_alb" "ecs_cluster_alb" {
  name            = "${var.ecs_cluster_name}-ALB"
  internal        = false
  security_groups = [aws_security_group.ecs_alb_security_group.id]
  subnets = split(
    ",",
    join(
      ",",
      data.terraform_remote_state.infrastructure.outputs.public_subnets,
    ),
  )

  tags = {
    Name = "${var.ecs_cluster_name}-ALB"
  }
}

resource "aws_alb_listener" "ecs_alb_https_listener" {
  load_balancer_arn = aws_alb.ecs_cluster_alb.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  // certificate_arn   = data.aws_acm_certificate.ecs_domain_certificate.arn
  certificate_arn   = data.aws_acm_certificate.ecs_domain_certificate.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.ecs_default_target_group.arn
  }

  depends_on = [aws_alb_target_group.ecs_default_target_group]
}

resource "aws_alb_target_group" "ecs_default_target_group" {
  name     = "${var.ecs_cluster_name}-TG"
  port     = 80
  protocol = "HTTP"
  vpc_id   = data.terraform_remote_state.infrastructure.outputs.vpc_id
  # health_check {
  #   path = "/administrator/"
  #   healthy_threshold = 5
  #   unhealthy_threshold = 2
  # }

  tags = {
    Name = "${var.ecs_cluster_name}-TG"
  }
}

resource "aws_lb_listener" "redirect-http-https" {
  load_balancer_arn = aws_alb.ecs_cluster_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }

  }
}

// move this to application side. as some users might have existing dmain name
// not sure what is the best way...
// resource "aws_route53_record" "ecs_load_balancer_record" {
//   name    = "*.${var.ecs_domain_name}"
//   type    = "A"
//   zone_id = data.aws_route53_zone.ecs_domain.zone_id

//   alias {
//     evaluate_target_health = false
//     name                   = aws_alb.ecs_cluster_alb.dns_name
//     zone_id                = aws_alb.ecs_cluster_alb.zone_id
//   }
// }

resource "aws_iam_role" "ecs_cluster_role" {
  name               = "${var.ecs_cluster_name}-IAM-Role"
  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
  {
    "Effect": "Allow",
    "Principal": {
      "Service": ["ecs.amazonaws.com", "ec2.amazonaws.com", "application-autoscaling.amazonaws.com"]
    },
    "Action": "sts:AssumeRole"
  }
  ]
}
EOF

}

resource "aws_iam_role_policy" "ecs_cluster_policy" {
  name   = "${var.ecs_cluster_name}-IAM-Policy"
  role   = aws_iam_role.ecs_cluster_role.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ecs:*",
        "ec2:*",
        "elasticloadbalancing:*",
        "ecr:*",
        "dynamodb:*",
        "cloudwatch:*",
        "s3:*",
        "rds:*",
        "sqs:*",
        "sns:*",
        "logs:*",
        "ssm:*"
      ],
      "Resource": "*"
    }
  ]
}
EOF

}