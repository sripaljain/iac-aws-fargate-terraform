variable "region" {
  default     = "ap-southeast-1"
  description = "AWS Region"
}

variable "project" {
  default = "TerraGrunt"
}

variable "environment" {
  type        = string
  default     = "dev"
  description = "Application enviroment"
}
variable "vpc_cidr" {
  default     = "10.0.0.0/16"
  description = "CIDR Block for VPC"
}


variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "3"
}

variable "public_subnet_tag_name" {
  type        = string
  default     = "TerragruntPUB"
  description = "Name tag for the public subnet"
}
variable "private_subnet_tag_name" {
  type        = string
  default     = "TerragruntPVT"
  description = "Name tag for the private subnet"
}

variable "availability_zones" {
  type        = list(string)
  default     = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
  description = "List of availability zones for the selected region"
}



# # variable "public_subnets_cidr" {
# #   type = list
# #   default = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]
# # }

# # variable "private_subnets_cidr" {
# #   type = list
# #   default = ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24"]
# # }